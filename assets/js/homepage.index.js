import Dropzone from 'dropzone';

require('../css/homepage.index.scss');

Dropzone.autoDiscover = false;
$(".dropzone").dropzone({
    url: "/file/new",
    addRemoveLinks: true,
    uploadMultiple: false,
    // paramName: refer to the input used to pass all files
    paramName: 'files',
    results: [],
    init: function () {
        const dz = this;
        // Get image names from template to generate thumbnail
        // serverId is defined to delete from server in removedfile events
        if (typeof images !== 'undefined') {
            $.each(images, function (index, value) {
                let thumb = {name: value, size: 0, dataURL: "/uploads/product/images/" + value, serverId: value};
                dz.files.push(thumb);
                dz.emit('addedfile', thumb);
                dz.createThumbnailFromUrl(thumb,
                    dz.options.thumbnailWidth, dz.options.thumbnailHeight,
                    dz.options.thumbnailMethod, true, function (thumbnail) {
                        dz.emit('thumbnail', thumb, thumbnail);
                    });
                dz.emit('complete', thumb);
            });
        }

        // When one image is uploaded, we push the generated name from back-end to an array
        this.on("success", function (file, response) {
            this.options.results.push(response.file);
            file.serverId = response.file;
        });

        // When every images inputs hidden are created for each image with the serverId
        // By security all previous inputs hidden are deleted and regen.
        // Images are uploaded but not linked into DB
        this.on("queuecomplete", function (file, response) {
            let merged = [].concat.apply([], this.options.results);
            $("input[class='name-files-upload']").remove();
            $.each(merged, function (index, value) {
                $('<input>').attr({
                    type: 'hidden',
                    id: value,
                    name: 'name-files-upload[' + index + ']',
                    value: value,
                    class: 'name-files-upload'
                }).appendTo('form');
            });
        });
    },
    // Deleted in frontend and back-end by post on event
    removedfile: function (file) {
        let name = file.serverId;
        let index = this.options.results.indexOf(name);

        $("input[id='" + name + "']").remove();

        if (index > -1) {
            this.options.results.splice(index, 1);
        }

        $.ajax({
            type: 'POST',
            url: '/file/delete',
            data: {name},
        }).done(function (msg) {});
        let _ref;
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
    }
});