<?php

namespace App\Service;


use App\Entity\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class FileUploader
{
    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file)
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        $file->move($this->getTargetDirectory(), $fileName);

        return $fileName;
    }

    public function remove(File $file)
    {
        $fileSystem = new Filesystem();
        $fileName = $file->getName();

        try {
            $fileSystem->remove($this->getTargetDirectory(). DIRECTORY_SEPARATOR .$fileName);
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while deleting your file ". $fileName ." at ".$exception->getPath();
        }
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}