<?php

namespace App\Controller\Homepage;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

/**
 * Class Index
 * @package App\Controller\Homepage
 */
final class Index
{
    /**
     * @var Environment $templating
     */
    private $templating;

    /**
     * Index constructor.
     * @param Environment $templating
     */
    public function __construct(Environment $templating)
    {
        $this->templating = $templating;
    }

    /**
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function __invoke()
    {
        return new Response($this->templating->render('homepage/index.html.twig', [
            'controller_name' => 'Index',
        ]));
    }
}
