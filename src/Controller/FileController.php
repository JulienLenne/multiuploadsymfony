<?php

namespace App\Controller;

use App\Entity\File;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FileUploader;

/**
 * @Route("/file")
 */
class FileController extends Controller
{
    /**
     * @Route("/new", name="file_new", methods="POST")
     * @param Request $request
     * @param FileUploader $fileUploader
     * @return JsonResponse
     */
    public function new(Request $request, FileUploader $fileUploader): JsonResponse
    {
        $names = [];
        $file = $request->files->get('files');
        dump($request->files);
        $em = $this->getDoctrine()->getManager();
        if ($file) {
                $fileModel = new File();
                $fileModel->setName($fileUploader->upload($file));
                $names[] = $fileModel->getName();
                $em->persist($fileModel);
        }
        $em->flush();

        return new JsonResponse(array('file' => $names));
    }

    /**
     * @Route("/delete", name="file_delete", methods="POST")
     * @param Request $request
     * @param FileUploader $fileUploader
     * @return JsonResponse
     */
    public function delete(Request $request, FileUploader $fileUploader): JsonResponse
    {
        dump($request);
        $filename = $request->request->get('name');
        $em = $this->getDoctrine()->getManager();
        $fileRepository = $em->getRepository(File::class);
        $file = $fileRepository->findOneBy(array('name' => $filename));
        //$fileUploader->remove($file);
        $em->remove($file);
        $em->flush();

        return new JsonResponse(array('deleted' => $filename));
    }
}
